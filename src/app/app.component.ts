import { DomSanitizer } from '@angular/platform-browser';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private sanitizer: DomSanitizer) {
    this.videoTag = this.getVideoTag();
  }
  title = 'zsp2-graduate-client';
  floatLabel = 'never';
  search = null;
  videoTag;

  getVideoTag() {
    return this
      .sanitizer
      .bypassSecurityTrustHtml(`<video *ngIf="true" muted autoplay loop id="background">
        <source src="assets/360_blur.mp4" type="video/mp4" />
        </video>
    `);
  }


}
