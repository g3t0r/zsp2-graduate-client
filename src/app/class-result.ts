import { Result } from './result';

export class ClassResult implements Result {
  constructor(
    public specialization: string = '',
    public yearOfSchoolCompletion: number = null,
    public photoUrl: string = '') { }

  // toPrettyString() {
  //   return `${this.specialization}, ${this.yearOfSchoolCompletion}`;
  // }
}
