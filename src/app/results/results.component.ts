import { ResultService } from './../result.service';
import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Result } from '../result';
import { DomSanitizer } from '@angular/platform-browser';
import { StudentResult } from '../student-result';
import { ClassResult } from '../class-result';
import { MatDialog } from '@angular/material';
import { ImageDialogComponent } from '../image-dialog/image-dialog.component';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})

export class ResultsComponent implements OnInit {

  constructor(private resultService: ResultService, public matDialog: MatDialog) { }

  private _search = null;
  @Input()
  set search(search: Result) {
    this._search = search || null;
  }

  get search() {
    return this._search;
  }
  results: Result[] = [];

  openPhoto(selectedResult: Result): void {
    const dialogRef = this.matDialog.open(ImageDialogComponent, {
      maxWidth: '95vw',
      maxHeight: '95vh',
      data: { photoUrl: selectedResult.photoUrl }
    });
  }

  getResults(search: Result): void {
    if (search instanceof ClassResult) {
      this.getClassResults(search);
    }
    if (search instanceof StudentResult) {
      this.getStudentResults(search);
    }
  }

  getStudentResults(search: StudentResult): void {
    this
      .resultService
      .getStudentResults(search.name, search.surname, search.specialization, search.yearOfSchoolCompletion)
      .subscribe(results => {
        this.results = results;
        this
          .results
          .forEach(function(part, index, array) {
            array[index] = Object.assign(new StudentResult, array[index]);
          });
      });

  }

  getClassResults(search: ClassResult): void {
    this
      .resultService
      .getClassResults(search.specialization, search.yearOfSchoolCompletion)
      .subscribe(results => {
        this.results = results;
        this
          .results
          .forEach(function(part, index, array) {
            array[index] = Object.assign(new ClassResult, array[index]);
          });
      });

  }

  createPrettyString(result: Result) {
    if (result instanceof ClassResult) {
      return `${result.specialization} ${result.yearOfSchoolCompletion}`;
    }
    if (result instanceof StudentResult) {
      return `${result.name} ${result.surname}` + ' ' +
        `${result.specialization} ${result.yearOfSchoolCompletion}`;
    }
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    this.getResults(this._search);
    console.log(this.createPrettyString(this._search));
  }

  toggleFullScreen() {
    const elem = document.body;
    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if (elem['msRequestFullscreen']) {
      elem['msRequestFullscreen']();
    } else if (elem['mozRequestFullScreen']) {
      elem['mozRequestFullScreen']();
    } else if (elem.webkitRequestFullscreen) {
      elem.webkitRequestFullscreen();
    }
  }



}
