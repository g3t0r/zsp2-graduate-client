import { Result } from './result';

export class StudentResult implements Result {
  constructor(
    public name: string = '',
    public surname: string = '',
    public specialization: string = '',
    public yearOfSchoolCompletion: number = null,
    public photoUrl: string = ''
  ) { }


  // toPrettyString() {
  //   return `${this.name} ${this.surname} ${this.specialization} ${this.yearOfSchoolCompletion}`;
  // }
}
