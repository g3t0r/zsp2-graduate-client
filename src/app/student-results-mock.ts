import { StudentResult } from './student-result';
export const STUDENT_RESULTS: StudentResult[] = [
  new StudentResult('Jan Tomasz', 'Kowalski', 'elektryk', 1995, './hehe'),
  new StudentResult('Jan Tomasz', 'Kowalski', 'informatyk', 1995, './hehe'),
  new StudentResult('Jan Tomasz', 'Kowalski', 'strażak', 1995, './hehe'),
  new StudentResult('Jan Tomasz', 'Kowalski', 'cyrkowiec', 1995, './hehe'),
  new StudentResult('Jan Tomasz', 'Kowalski', 'dyrektor', 1995, './hehe'),
  new StudentResult('Jan Tomasz', 'Kowalski', 'cwaniak', 1995, './hehe'),
  new StudentResult('Jan Tomasz', 'Kowalski', 'akrobata', 1995, './hehe')
];
