import { Result } from './../result';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { StudentResult } from './../student-result';
import { ClassResult } from '../class-result';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {

  constructor() { }

  @Output() searched = new EventEmitter<Result>();
  @Output() home = new EventEmitter<boolean>();
  searchBarData = new StudentResult();
  squered = true;

  goHome() {
    console.log('going home');
    this.home.emit(true);
    document.getElementById('background')['play']();
    this.squered = true;
    this.searchBarData.name = '';
    this.searchBarData.surname = '';
    this.searchBarData.specialization = '';
    this.searchBarData.yearOfSchoolCompletion = null;
  }

  checkIfEnter(event) {
    if (event.key === 'Enter') {
      this.passFormData();
    }
  }

  passFormData() {
    if (!this.fieldsAreEmpty()) {
      document.getElementById('background')['pause']();
      this.squered = false;
      if (this.isSearchForStudent()) {
        this.searched.emit(new StudentResult(
          this.searchBarData.name,
          this.searchBarData.surname,
          this.searchBarData.specialization,
          this.searchBarData.yearOfSchoolCompletion
        ));
      } else {
        this.searched.emit(new ClassResult(
          this.searchBarData.specialization,
          this.searchBarData.yearOfSchoolCompletion
        ));
      }
    }
  }

  ngOnInit() {
  }

  fieldsAreEmpty(): boolean {
    return this.searchBarData.name === '' &&
      this.searchBarData.surname === '' &&
      this.searchBarData.specialization === '' &&
      this.searchBarData.yearOfSchoolCompletion == null;
  }

  isSearchForStudent() {
    return this.searchBarData.name !== '' ||
      this.searchBarData.surname !== '';
  }

}
