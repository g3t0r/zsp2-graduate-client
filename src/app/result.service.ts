import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ClassResult } from './class-result';
import { CLASS_RESULTS } from './class-results-mock';
import { StudentResult } from './student-result';
import { STUDENT_RESULTS } from './student-results-mock';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';


const BASIC_STUDENT_URL = 'http://localhost/api/student-result.php?';
const BASIC_CLASS_URL = 'http://localhost/api/class-result.php?';

@Injectable({
  providedIn: 'root'
})
export class ResultService {

  constructor(private http: HttpClient) { }


  getStudentResults(name: string, surname: string, specialization: string, yearOfSchoolCompletion: number): Observable<StudentResult[]> {
    let url = BASIC_STUDENT_URL;

    if (name !== '') {
      url += `name=${name}&`;
    }

    if (surname !== '') {
      url += `surname=${surname}&`;
    }

    if (specialization !== '') {
      url += `specialization=${specialization}&`;
    }

    if (yearOfSchoolCompletion !== null) {
      url += `yearOfSchoolCompletion=${yearOfSchoolCompletion}&`;
    }

    return this
      .http
      .get<StudentResult[]>(url);

  }

  getClassResults(specialization: string, yearOfSchoolCompletion: number): Observable<ClassResult[]> {
    let url = BASIC_CLASS_URL;

    if (specialization !== '') {
      url += `specialization=${specialization}&`;
    }

    if (yearOfSchoolCompletion !== null) {
      console.log(`ResultService:getClassResults:yearOfSchoolCompletion=${yearOfSchoolCompletion}`);
      url += `yearOfSchoolCompletion=${yearOfSchoolCompletion}&`;
    }

    return this
      .http
      .get<ClassResult[]>(url);
  }
}
