import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatToolbarModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';


@NgModule({
    imports: [
        CommonModule,
        MatToolbarModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatListModule,
        MatButtonModule,
        MatDialogModule
    ],
    exports: [
        MatToolbarModule,
        MatInputModule,
        BrowserAnimationsModule,
        MatListModule,
        MatButtonModule,
        MatDialogModule
    ],
    declarations: []
})
export class MaterialCustomModule { }
